package ru.env.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.env.DTO.PositionDto;
import ru.env.entity.Position;
import ru.env.entity.User;
import ru.env.mapper.PositionMapper;
import ru.env.service.PositionService;
import ru.env.service.UserService;

import java.util.List;
import java.util.Optional;

@RestController
public class PositionController {

    @Autowired
    PositionService positionService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/getProduct/{positionId}", method = RequestMethod.GET)
    public ResponseEntity<PositionDto> getProduct(@PathVariable int positionId) {
        User user = userService.getActiveUser();
        Position position =  positionService.getPosition(positionId);
        Optional<PositionDto> dto = Optional.empty();
        if (position != null) {
            dto = Optional.ofNullable(PositionMapper.positionToDto(position, user));
        }
        return ResponseEntity.of(dto);
    }

    @RequestMapping(value = "/getAllPositions", method = RequestMethod.GET)
    public ResponseEntity<List<PositionDto>> getAllProducts() {
        User user = userService.getActiveUser();
        List<Position> positions = positionService.findAll();
        return ResponseEntity.ok(PositionMapper.positionListToDto(positions, user));
    }

    @RequestMapping(value = "/getSimilarProductOffers/{positionId}", method = RequestMethod.GET)
    public ResponseEntity<List<PositionDto>> getSimilarProductOffers(@PathVariable int positionId) {
        User user = userService.getActiveUser();
        List<Position> positions = positionService.getSimilarOffers(positionId);
        if (positions == null) {
            return ResponseEntity.of(Optional.empty());
        }
        List<PositionDto> dtoList = PositionMapper.positionListToDto(positions, user);
        return ResponseEntity.ok(dtoList);
    }

    @RequestMapping(value = "/search")
    public ResponseEntity<List<PositionDto>> searchPositionsByNamePattern(@RequestParam String q) {
        User user = userService.getActiveUser();
        List<Position> positions = positionService.getPositionsByNamePattern(q);
        if (positions == null) {
            return ResponseEntity.of(Optional.empty());
        }
        List<PositionDto> dtoList = PositionMapper.positionListToDto(positions, user);
        return ResponseEntity.ok(dtoList);
    }

}
