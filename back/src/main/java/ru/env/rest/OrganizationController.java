package ru.env.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.env.DTO.OrganizationDto;
import ru.env.entity.Organization;
import ru.env.mapper.OrganizationMapper;
import ru.env.service.OrganizationService;

import java.util.Optional;

@RestController
public class OrganizationController {

    @Autowired
    OrganizationService organizationService;

    @RequestMapping(value = "/organization/{organizationId}")
    public ResponseEntity<OrganizationDto> searchPositionsByNamePattern(@PathVariable int organizationId) {
        Organization organization = organizationService.findOne(organizationId);
        if (organization == null) {
            return ResponseEntity.of(Optional.empty());
        }
        return ResponseEntity.ok(OrganizationMapper.organizationToDto(organization));
    }

}
