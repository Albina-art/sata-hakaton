package ru.env.entity;

import java.util.HashSet;
import java.util.Set;

public class User {

    int userId;
    Set<Integer> bookmarkedPositionsId = new HashSet<>();

    public int getUserId() {
        return userId;
    }

    public User setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public Set<Integer> getBookmarkedPositionsId() {
        return bookmarkedPositionsId;
    }

    public User setBookmarkedPositionsId(Set<Integer> bookmarkedPositionsId) {
        this.bookmarkedPositionsId = bookmarkedPositionsId;
        return this;
    }
}
