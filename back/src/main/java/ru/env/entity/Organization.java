package ru.env.entity;

import ru.env.utils.Location;

import java.util.Set;

public class Organization {

    private int id;
    private String name;
    private OrganizationType organizationType;
    private Set<Service> services;
    private double organizationScore;
    private Location location;
    private Trust trust; // надежность
    private int clients;
    private String description;
    private boolean protectedDealEnabled; // риски
    private String imageSrc;

    public enum Trust {
        LOW, MEDIUM, HIGH
    }

    public enum OrganizationType {
        IP("ИП"), SAM("Самозанятый"), ORG("Юридическое лицо");

        String name;

        OrganizationType(String name) {
            this.name = name;
        }
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public Organization setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
        return this;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public Organization setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
        return this;
    }

    public boolean isProtectedDealEnabled() {
        return protectedDealEnabled;
    }

    public Organization setProtectedDealEnabled(boolean protectedDealEnabled) {
        this.protectedDealEnabled = protectedDealEnabled;
        return this;
    }

    public int getId() {
        return id;
    }

    public Organization setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Organization setName(String name) {
        this.name = name;
        return this;
    }

    public Set<Service> getServices() {
        return services;
    }

    public Organization setServices(Set<Service> services) {
        this.services = services;
        return this;
    }

    public double getOrganizationScore() {
        return organizationScore;
    }

    public Organization setOrganizationScore(double organizationScore) {
        this.organizationScore = organizationScore;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Organization setLocation(Location location) {
        this.location = location;
        return this;
    }

    public Trust getTrust() {
        return trust;
    }

    public Organization setTrust(Trust trust) {
        this.trust = trust;
        return this;
    }

    public int getClients() {
        return clients;
    }

    public Organization setClients(int clients) {
        this.clients = clients;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Organization setDescription(String description) {
        this.description = description;
        return this;
    }
}
