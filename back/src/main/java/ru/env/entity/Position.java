package ru.env.entity;

public class Position {

    private int id;
    private Organization organization;
    private Product product;
    private int price;
    private double positionScore;

    public double getPositionScore() {
        return positionScore;
    }

    public Position setPositionScore(double positionScore) {
        this.positionScore = positionScore;
        return this;
    }

    public int getId() {
        return id;
    }

    public Position setId(int id) {
        this.id = id;
        return this;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Position setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public Product getProduct() {
        return product;
    }

    public Position setProduct(Product product) {
        this.product = product;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Position setPrice(int price) {
        this.price = price;
        return this;
    }
}
