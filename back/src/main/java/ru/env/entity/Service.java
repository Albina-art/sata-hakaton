package ru.env.entity;

public enum  Service {

    SLESARKA(0, "Слесарка"),
    STOLYARKA(1, "Столярка"),
    SVARKA(2, "Сварка")
    ;

    Service(int i, String name) {
        this.i = i;
        this.name = name;
    }

    public final int i;
    public final String name;

}
