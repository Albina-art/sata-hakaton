package ru.env.entity;

import java.util.List;

public class Product {

    private int id;
    private String name;
    private String imageSrc;
    private List<String> properties;
    private String description;
    private Service service;
    private ProductType productType;

    public enum ProductType {
        SERVICE, PRODUCT
    }

    public ProductType getProductType() {
        return productType;
    }

    public Product setProductType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public Service getService() {
        return service;
    }

    public Product setService(Service service) {
        this.service = service;
        return this;
    }

    public int getId() {
        return id;
    }

    public Product setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public Product setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
        return this;
    }

    public List<String> getProperties() {
        return properties;
    }

    public Product setProperties(List<String> properties) {
        this.properties = properties;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }
}
