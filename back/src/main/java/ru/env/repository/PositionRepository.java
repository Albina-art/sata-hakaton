package ru.env.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.env.database.MockedDatabase;
import ru.env.entity.Organization;
import ru.env.entity.Position;
import ru.env.entity.Product;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PositionRepository {

    @Autowired
    MockedDatabase repository;

    public Position findOne(int positionId) {
        return repository.positions.get(positionId);
    }

    public List<Position> findAll() {
        return new ArrayList<>(repository.positions.values());
    }
}
