package ru.env.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.env.database.MockedDatabase;
import ru.env.entity.User;

@Repository
public class UserRepository {

    @Autowired
    MockedDatabase mockedDatabase;

    public User findOne(int userId) {
        return mockedDatabase.users.get(userId);
    }
}
