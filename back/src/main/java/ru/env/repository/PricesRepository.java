package ru.env.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.env.database.MockedDatabase;

@Repository
public class PricesRepository {

    @Autowired
    MockedDatabase mockedDatabase;

    public int findOne(int productId) {
        return mockedDatabase.maxPricesForProductIds.get(productId);
    }
}
