package ru.env.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.env.database.MockedDatabase;
import ru.env.entity.Organization;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrganizationRepository {

    @Autowired
    MockedDatabase repository;

    public Organization findOne(int organizationId) {
        return repository.organizations.get(organizationId);
    }

    public List<Organization> findAll() {
        return new ArrayList<>(repository.organizations.values());
    }
}
