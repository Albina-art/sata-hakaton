package ru.env.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.env.database.MockedDatabase;
import ru.env.entity.Organization;
import ru.env.entity.Position;
import ru.env.entity.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepository {

    @Autowired
    MockedDatabase repository;

    public Product findOne(int productId) {
        return repository.products.get(productId);
    }

    public List<Product> findAll() {
        return new ArrayList<>(repository.products.values());
    }



}
