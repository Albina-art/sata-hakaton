package ru.env.mapper;

import ru.env.DTO.PositionDto;
import ru.env.entity.Position;
import ru.env.entity.User;

import java.util.ArrayList;
import java.util.List;

public class PositionMapper {

    public static List<PositionDto> positionListToDto(List<Position> positions, User user) {
        List<PositionDto> dtoList = new ArrayList<>();
        PositionDto dto;

        for (Position position : positions) {
            dto = positionToDto(position, user);
            dtoList.add(dto);
        }
        return dtoList;
    }

        public static PositionDto positionToDto(Position position, User user) {
        PositionDto dto = new PositionDto()
                .setPositionId(position.getId())
                .setProductId(position.getProduct().getId())
                .setOrganizationId(position.getOrganization().getId())
                .setPrice(position.getPrice())
                .setProductImageSrc(position.getProduct().getImageSrc())
                .setProductName(position.getProduct().getName())
                .setProperties(position.getProduct().getProperties())
                .setProtectedDeal(position.getOrganization().isProtectedDealEnabled())
                .setOrganizationType(position.getOrganization().getOrganizationType())
                .setProductDescription(position.getProduct().getDescription())
                .setClientCount(position.getOrganization().getClients())
                .setTrust(position.getOrganization().getTrust())
                .setOrganizationImageSrc(position.getOrganization().getImageSrc())
                .setScore(position.getPositionScore())
                .setOrganizationName(position.getOrganization().getName())
                .setLocation(position.getOrganization().getLocation().city)
                .setOrganizationDescription(position.getOrganization().getDescription())
                .setProductType(position.getProduct().getProductType());
            if (user.getBookmarkedPositionsId().contains(position.getId())) {
                dto.setBookmarks(true);
            }
        return dto;
    }
}
