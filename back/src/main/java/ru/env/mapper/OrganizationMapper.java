package ru.env.mapper;

import ru.env.DTO.OrganizationDto;
import ru.env.DTO.PositionDto;
import ru.env.entity.Organization;
import ru.env.entity.Position;
import ru.env.entity.Service;
import ru.env.entity.User;
import ru.env.utils.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class OrganizationMapper {

    public static List<OrganizationDto> organizationListToDto(List<Organization> organizations) {
        List<OrganizationDto> dtoList = new ArrayList<>();
        OrganizationDto dto;

        for (Organization organization : organizations) {
            dto = organizationToDto(organization);
            dtoList.add(dto);
        }
        return dtoList;
    }

        public static OrganizationDto organizationToDto(Organization organization) {
        OrganizationDto dto = new OrganizationDto()
                .setId(organization.getId())
                .setName(organization.getName())
                .setOrganizationType(organization.getOrganizationType())
                .setServices(organization.getServices())
                .setOrganizationScore(organization.getOrganizationScore())
                .setLocation(organization.getLocation().city)
                .setTrust(organization.getTrust())
                .setClientCount(organization.getClients())
                .setDescription(organization.getDescription())
                .setProtectedDealEnabled(organization.isProtectedDealEnabled())
                .setImageSrc(organization.getImageSrc());
        return dto;
    }
}
