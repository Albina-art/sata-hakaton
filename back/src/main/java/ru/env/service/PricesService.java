package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.entity.Position;
import ru.env.entity.Product;
import ru.env.repository.PricesRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PricesService {

    @Autowired
    ProductService productService;

    @Autowired
    PositionService positionService;

    @Autowired
    PricesRepository pricesRepository;

    public Map<Integer, Integer> getMaxPricesForProductIds() {
        Map<Integer, Integer> map = new HashMap<>();
        List<Product> products = productService.findAll();
        Optional<Integer> max;
        List<Position> positions;
        for (Product product : products) {
            positions = positionService.findAllByProductId(product.getId());
            max = positions.stream().map(Position::getPrice).max(Comparator.comparing(Integer::valueOf));
            max.ifPresent(integer -> map.put(product.getId(), integer));
        }
        return map;
    }


    public int getMaxPriceForProductId(int productId) {
        return pricesRepository.findOne(productId);
    }
}
