package ru.env.service;

import org.springframework.stereotype.Service;
import ru.env.entity.Organization;

@Service
public class MockLoyaltyService {
    public double getLoyaltyScore(Organization organization) {
        //max for new users and ones with high money turnover
        return organization.getId() * 31 * 31 * 31 % 10 / 10.0;
    }
}
