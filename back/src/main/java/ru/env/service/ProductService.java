package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.entity.Product;
import ru.env.repository.ProductRepository;

import java.util.*;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PositionService positionService;

    public Product getProduct(int productId) {
        return productRepository.findOne(productId);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> findAllByNamePattern(String stringPattern) {
        List<Product> fullList = productRepository.findAll();
        List<Product> out = new ArrayList<>();
        for (Product product : fullList) {
            if (product.getName().toLowerCase().matches(".*" + stringPattern + ".*")) {
                out.add(product);
            }
        }
        return out;
    }
}
