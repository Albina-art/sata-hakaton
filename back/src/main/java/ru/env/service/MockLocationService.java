package ru.env.service;

import org.springframework.stereotype.Service;
import ru.env.entity.Organization;

@Service
public class MockLocationService {

    public int getDistance(Organization organization) {
        return 1;
    }

}
