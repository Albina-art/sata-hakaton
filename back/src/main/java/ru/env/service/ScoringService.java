package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.entity.Organization;
import ru.env.entity.Position;

@Service
public class ScoringService {

    public static final int MAX_DISTANCE_WITHOUT_PENALTY = 5000; // in meters
    public static final int MAX_DISTANCE = 100000; // in meters

    @Autowired
    MockDueDiliganceService mockDueDiliganceService;

    @Autowired
    MockLocationService locationService;

    @Autowired
    MockReviewService reviewService;

    @Autowired
    MockLoyaltyService loyaltyService;

    @Autowired
    PricesService pricesService;

    public double getDistanceScore(Organization organization) {
        int distance = locationService.getDistance(organization);
        if (distance > MAX_DISTANCE) {
            return 0;
        } else if (distance < MAX_DISTANCE_WITHOUT_PENALTY) {
            return 1;
        } else {
            return (MAX_DISTANCE - distance) / MAX_DISTANCE;
        }
    }

    public double getTrustScore(Organization organization) {
        return mockDueDiliganceService.getTrust(organization);
    }

    public double getReviewScore(Organization organization) {
        return reviewService.getScore(organization);
    }

    public double getLoyaltyScore(Organization organization) {
        return loyaltyService.getLoyaltyScore(organization);
    }

    public double getPriceScore(Position position) {
        return position.getPrice() / pricesService.getMaxPriceForProductId(position.getProduct().getId());
    }
}
