package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.database.MockedDatabase;
import ru.env.entity.User;
import ru.env.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User getActiveUser() {
        return userRepository.findOne(MockedDatabase.MOCKED_USER_ID);
    }
}
