package ru.env.service;

import org.springframework.stereotype.Repository;
import ru.env.entity.Organization;


@Repository
public class MockDueDiliganceService {

    public boolean isInBlackList(Organization organization) {
        return false;
    }

    public double getTrust(Organization organization) {
        return organization.getId() * 31 % 10 / 10.0;
    }
}
