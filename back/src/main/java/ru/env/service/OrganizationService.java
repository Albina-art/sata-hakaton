package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.entity.Organization;
import ru.env.repository.OrganizationRepository;
import ru.env.utils.OrganizationScoring;

import java.util.List;

@Service
public class OrganizationService {

    @Autowired
    OrganizationRepository organizationRepository;

    @Autowired
    ScoringService scoringService;

    public Organization findOne(int organizationId) {
        return organizationRepository.findOne(organizationId);
    }

    public List<Organization> findAll() {
        return organizationRepository.findAll();
    }

    public void updateOrganizationScore(Organization organization) {
        double scoring = OrganizationScoring.DISTANCE.weight * scoringService.getDistanceScore(organization)
                + OrganizationScoring.DUE_DILIGENCE_TRUST.weight * scoringService.getTrustScore(organization)
                + OrganizationScoring.REVIEWS_RATING.weight * scoringService.getReviewScore(organization)
                + OrganizationScoring.USER_LOYALTY.weight + scoringService.getLoyaltyScore(organization);
        organization.setOrganizationScore(scoring);
    }

}
