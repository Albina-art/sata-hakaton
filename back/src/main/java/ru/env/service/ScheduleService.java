package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.env.database.MockedDatabase;
import ru.env.entity.Organization;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ScheduleService {

    @Autowired
    OrganizationService organizationService;

    @Autowired
    PricesService pricesService;

    @Autowired
    MockedDatabase mockedDatabase;

    @PostConstruct
    public void init() {
        updatePriceScorings();
    }

    @Scheduled (cron = "0 0 3 * * ?")
    public void updateScorings() {
        updateOrganizationScoring();
        updatePriceScorings();
    }

    public void updateOrganizationScoring() {
        List<Organization> organizationList = organizationService.findAll();
        organizationList.forEach(organizationService::updateOrganizationScore);
    }

    public void updatePriceScorings() {
        mockedDatabase.maxPricesForProductIds = pricesService.getMaxPricesForProductIds();
    }
}
