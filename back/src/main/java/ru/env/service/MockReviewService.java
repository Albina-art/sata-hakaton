package ru.env.service;

import org.springframework.stereotype.Service;
import ru.env.entity.Organization;

@Service
public class MockReviewService {
    public double getScore(Organization organization) {
        return organization.getId() * 31 * 31 % 10 / 10.0;
    }
}
