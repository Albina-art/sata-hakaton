package ru.env.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.env.entity.Organization;
import ru.env.entity.Position;
import ru.env.entity.Product;
import ru.env.repository.PositionRepository;
import ru.env.utils.PositionScoring;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PositionService {

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    MockDueDiliganceService mockDueDiliganceService;

    @Autowired
    ScoringService scoringService;

    @Autowired
    ProductService productService;

    public Position getPosition(int positionId) {
        return positionRepository.findOne(positionId);
    }

    public List<Position> findAll() {
        List<Position> positionList = positionRepository.findAll()
                .stream()
                .filter(x -> !isInBlackList(x.getOrganization()))
                .collect(Collectors.toList());
        positionList.forEach(this::updatePositionScore);
        positionList.sort((x, y) -> (int) (100 * (y.getPositionScore() - x.getPositionScore())));
        return positionRepository.findAll();
    }

    public List<Position> findAllByProductId(int productId) {
        List<Position> positions = positionRepository.findAll();
        return positions.stream().filter(x -> x.getProduct().getId() == productId).collect(Collectors.toList());
    }

    public List<Position> getSimilarOffers(int positionId) {
        Position position = positionRepository.findOne(positionId);
        if (position == null) {
            return null;
        }
        List<Position> positionList = positionRepository.findAll()
                .stream()
                .filter(x -> !isInBlackList(x.getOrganization()))
                .filter(x -> x.getId() != positionId)
                .filter(x -> x.getProduct().getId() == position.getProduct().getId())
                .collect(Collectors.toList());
        positionList.forEach(this::updatePositionScore);
        positionList.sort((x, y) -> (int) (100 * (y.getPositionScore() - x.getPositionScore())));
        return positionList;
    }

    private void updatePositionScore(Position position) {
        double score = PositionScoring.PRICE.weight * scoringService.getPriceScore(position)
                + PositionScoring.ORGANIZATION_SCORE.weight * position.getOrganization().getOrganizationScore();
        position.setPositionScore(score);
    }

    private boolean isInBlackList(Organization organization) {
        return mockDueDiliganceService.isInBlackList(organization);
    }

    public List<Position> getPositionsByNamePattern(String pattern) {
        List<Product> products = productService.findAllByNamePattern(pattern.toLowerCase());
        return getOffersByProduct(products);
    }

    private List<Position> getOffersByProduct(List<Product> products) {
        List<Position> positionList = positionRepository.findAll();
        List<Position> out = new ArrayList<>();
        for (Product product : products) {
             out.addAll(
                     positionList.stream()
                    .filter(x -> !isInBlackList(x.getOrganization()))
                    .filter(x -> x.getProduct().getId() == product.getId())
                    .collect(Collectors.toList())
             );
        }
        positionList.forEach(this::updatePositionScore);
        out.sort((x, y) -> (int) (100 * (y.getPositionScore() - x.getPositionScore())));
        return out;
    }
}
