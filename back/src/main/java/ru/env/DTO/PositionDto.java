package ru.env.DTO;

import ru.env.entity.Organization;
import ru.env.entity.Product;

import java.util.List;

public class PositionDto {

    private int positionId;
    private int productId;
    private int organizationId;

    private String productName;
    private String organizationName;
    private String productDescription;
    private String organizationDescription;

    private String productImageSrc;
    private String organizationImageSrc;

    private Product.ProductType productType;
    private Organization.OrganizationType organizationType;

    private Organization.Trust trust;
    private boolean isProtectedDeal;
    private int clientCount;
    private String location;

    private boolean bookmarks;
    private List<String> properties;
    private int price;
    private double score;


    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public PositionDto setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
        return this;
    }

    public Product.ProductType getProductType() {
        return productType;
    }

    public PositionDto setProductType(Product.ProductType productType) {
        this.productType = productType;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public PositionDto setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getOrganizationImageSrc() {
        return organizationImageSrc;
    }

    public PositionDto setOrganizationImageSrc(String organizationImageSrc) {
        this.organizationImageSrc = organizationImageSrc;
        return this;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public PositionDto setProductDescription(String productDescription) {
        this.productDescription = productDescription;
        return this;
    }

    public int getClientCount() {
        return clientCount;
    }

    public PositionDto setClientCount(int clientCount) {
        this.clientCount = clientCount;
        return this;
    }

    public Organization.Trust getTrust() {
        return trust;
    }

    public PositionDto setTrust(Organization.Trust trust) {
        this.trust = trust;
        return this;
    }

    public boolean isBookmarks() {
        return bookmarks;
    }

    public PositionDto setBookmarks(boolean bookmarks) {
        this.bookmarks = bookmarks;
        return this;
    }

    public double getScore() {
        return score;
    }

    public PositionDto setScore(double score) {
        this.score = score;
        return this;
    }

    public Organization.OrganizationType getOrganizationType() {
        return organizationType;
    }

    public PositionDto setOrganizationType(Organization.OrganizationType organizationType) {
        this.organizationType = organizationType;
        return this;
    }

    public int getPositionId() {
        return positionId;
    }

    public PositionDto setPositionId(int positionId) {
        this.positionId = positionId;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public PositionDto setPrice(int price) {
        this.price = price;
        return this;
    }

    public String getProductImageSrc() {
        return productImageSrc;
    }

    public PositionDto setProductImageSrc(String productImageSrc) {
        this.productImageSrc = productImageSrc;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public PositionDto setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public int getProductId() {
        return productId;
    }

    public PositionDto setProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public PositionDto setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
        return this;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public PositionDto setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public List<String> getProperties() {
        return properties;
    }

    public PositionDto setProperties(List<String> properties) {
        this.properties = properties;
        return this;
    }

    public boolean isProtectedDeal() {
        return isProtectedDeal;
    }

    public PositionDto setProtectedDeal(boolean protectedDeal) {
        isProtectedDeal = protectedDeal;
        return this;
    }
}
