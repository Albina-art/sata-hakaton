package ru.env.DTO;

import ru.env.entity.Organization;
import ru.env.entity.Service;
import ru.env.utils.Location;

import java.util.Set;

public class OrganizationDto {

    private int id;
    private String name;
    private Organization.OrganizationType organizationType;
    private Set<Service> services;
    private double organizationScore;
    private String location;
    private Organization.Trust trust;
    private int clientCount;
    private String description;
    private boolean protectedDealEnabled;
    private String imageSrc;

    public int getId() {
        return id;
    }

    public OrganizationDto setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public OrganizationDto setName(String name) {
        this.name = name;
        return this;
    }

    public Organization.OrganizationType getOrganizationType() {
        return organizationType;
    }

    public OrganizationDto setOrganizationType(Organization.OrganizationType organizationType) {
        this.organizationType = organizationType;
        return this;
    }

    public Set<Service> getServices() {
        return services;
    }

    public OrganizationDto setServices(Set<Service> services) {
        this.services = services;
        return this;
    }

    public double getOrganizationScore() {
        return organizationScore;
    }

    public OrganizationDto setOrganizationScore(double organizationScore) {
        this.organizationScore = organizationScore;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public OrganizationDto setLocation(String location) {
        this.location = location;
        return this;
    }

    public Organization.Trust getTrust() {
        return trust;
    }

    public OrganizationDto setTrust(Organization.Trust trust) {
        this.trust = trust;
        return this;
    }

    public int getClientCount() {
        return clientCount;
    }

    public OrganizationDto setClientCount(int clientCount) {
        this.clientCount = clientCount;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OrganizationDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isProtectedDealEnabled() {
        return protectedDealEnabled;
    }

    public OrganizationDto setProtectedDealEnabled(boolean protectedDealEnabled) {
        this.protectedDealEnabled = protectedDealEnabled;
        return this;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public OrganizationDto setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
        return this;
    }
}
