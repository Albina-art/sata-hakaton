package ru.env.database;

import org.springframework.stereotype.Component;
import ru.env.entity.*;
import ru.env.utils.Location;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class MockedDatabase {

    public static final int MOCKED_USER_ID = 1;

    public Map<Integer, Organization> organizations = new HashMap<>();
    public Map<Integer, Product> products = new HashMap<>();
    public Map<Integer, Service> services = new HashMap<>();
    public Map<Integer, Position> positions = new HashMap<>();
    public Map<Integer, User> users = new HashMap<>();
    public Map<Integer, Integer> maxPricesForProductIds = new HashMap<>();

    @PostConstruct
    private void init() {
        mockUser();
        mockServices();
        mockOrganizations();
        mockProducts();
        mockPositions();
    }

    private void mockUser() {
        users.put(MOCKED_USER_ID, new User().setUserId(MOCKED_USER_ID));
    }

    private void mockServices() {
        services.put(Service.SLESARKA.i, Service.SLESARKA);
        services.put(Service.STOLYARKA.i, Service.STOLYARKA);
        services.put(Service.SVARKA.i, Service.SVARKA);
    }

    private void mockOrganizations() {
        int i = 0;
        HashSet<Service> serviceHashSet = new HashSet<>();

        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("Мастерская Михалыча")
                .setClients(3)
                .setDescription("Слесарка и столярка от Михалыча - номер один в Москве")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.HIGH)
                .setOrganizationType(Organization.OrganizationType.IP)
                .setProtectedDealEnabled(false)
                .setOrganizationScore(0.63)
                .setLocation(new Location(1.0, 1.0, "Воркута"))
                .setImageSrc("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Flam.lesser.600pix_%28Pingstone%29.jpg/275px-Flam.lesser.600pix_%28Pingstone%29.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("SATA")
                .setClients(42)
                .setDescription("Отложим сварку на потом")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.HIGH)
                .setOrganizationType(Organization.OrganizationType.SAM)
                .setProtectedDealEnabled(true)
                .setOrganizationScore(0.93)
                .setLocation(new Location(1.0, 1.0, "Москва"))
                .setImageSrc("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Flam.lesser.600pix_%28Pingstone%29.jpg/275px-Flam.lesser.600pix_%28Pingstone%29.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("Подвал Петровича")
                .setClients(0)
                .setDescription("Если нужно хоть что-то")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.LOW)
                .setOrganizationType(Organization.OrganizationType.SAM)
                .setProtectedDealEnabled(true)
                .setOrganizationScore(0.04)
                .setLocation(new Location(1.0, 1.0, "Бийск"))
                .setImageSrc("https://i.ytimg.com/vi/hRODy67ATn0/maxresdefault.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("Руки из")
                .setClients(5)
                .setDescription("Вы нас недооцениваете")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.MEDIUM)
                .setOrganizationType(Organization.OrganizationType.IP)
                .setProtectedDealEnabled(false)
                .setOrganizationScore(0.34)
                .setLocation(new Location(1.0, 1.0, "Сыктывкар"))
                .setImageSrc("http://simple-fauna.ru/wp-content/uploads/2018/01/zhiraf.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("OOO \"Сварной\"")
                .setClients(41)
                .setDescription("Сварим, даже если Вы и против!")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.MEDIUM)
                .setOrganizationType(Organization.OrganizationType.ORG)
                .setProtectedDealEnabled(true)
                .setOrganizationScore(0.44)
                .setLocation(new Location(1.0, 1.0, "Москва"))
                .setImageSrc("https://www.born-spb.ru/images/svarka-452565.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("Самозанятые и ИП в ООО \"Сварной\"")
                .setClients(18)
                .setDescription("Если вы пользовались услугами ООО \"Сварной\", то Вы уже с нами работали!")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.HIGH)
                .setOrganizationType(Organization.OrganizationType.IP)
                .setProtectedDealEnabled(false)
                .setOrganizationScore(0.84)
                .setLocation(new Location(1.0, 1.0, "Москва"))
                .setImageSrc("https://svarkagid.ru/wp-content/uploads/2016/12/svarka-nerzhavejki-poluavtomatom-2.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("ООО \"Зеленоглазое такси\"")
                .setClients(11)
                .setDescription("Когда-то наше бюро было знаменито своей сваркой, но теперь мы переходим на столярное дело. И такси")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.MEDIUM)
                .setOrganizationType(Organization.OrganizationType.ORG)
                .setProtectedDealEnabled(true)
                .setOrganizationScore(0.45)
                .setLocation(new Location(1.0, 1.0, "Караганда"))
                .setImageSrc("https://svarkagid.ru/wp-content/uploads/2016/12/svarka-nerzhavejki-poluavtomatom-2.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("ООО \"Сварка экспресс\"")
                .setClients(43)
                .setDescription("Работаем сварщиками с 1221 года")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.MEDIUM)
                .setOrganizationType(Organization.OrganizationType.ORG)
                .setProtectedDealEnabled(true)
                .setOrganizationScore(0.77)
                .setLocation(new Location(1.0, 1.0, "Нижний Новгород"))
                .setImageSrc("https://nochdobra.com/wp-content/uploads/2017/03/www.GetBg_.net_Photoshop_Cat_in_Space_043860_-1320x743.jpg")
        );

        serviceHashSet = new HashSet<>();
        serviceHashSet.add(Service.SLESARKA);
        serviceHashSet.add(Service.STOLYARKA);
        serviceHashSet.add(Service.SVARKA);
        organizations.put(i, new Organization()
                .setId(i++)
                .setName("Шишкин Л.Е.")
                .setClients(2)
                .setDescription("Ай да сварка, ай да гарантия!")
                .setServices(serviceHashSet)
                .setTrust(Organization.Trust.MEDIUM)
                .setOrganizationType(Organization.OrganizationType.IP)
                .setProtectedDealEnabled(false)
                .setOrganizationScore(0.27)
                .setLocation(new Location(1.0, 1.0, "Москва"))
                .setImageSrc("https://upload.wikimedia.org/wikipedia/commons/9/9f/%D0%A8%D0%B8%D1%88%D0%BA%D0%B8_%D0%BB%D0%B8%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%B8%D1%86%D1%8B_%D0%BB%D0%B5%D1%82%D0%BE%D0%BC.jpg")
        );

    }

    private void mockProducts() {
        int i = 0;

        //Столярка
        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.STOLYARKA)
                .setId(i++)
                .setName("Брусок строганый 50х50х3000 мм хвоя")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/81947945.jpg")
                .setDescription("Брусок строганый 3 м из массива ели обыкновенной без обработки используется в строительной сфере для возведения жилых домов, ремонта кровли, создания каркасов, изготовления корпусной мебели. Представляет собой пиломатериал из хвойной древесины с мелкими узлами. Брусок прост в обработке, не деформируется под воздействием погодных условий и перепадов температур.")
                .setProperties(Arrays.asList("Длина заготовки — 3 м", "Размер сечения, имеющего форму квадрата, — 50х50 мм"))
                );

        products.put(i, new Product()
                        .setProductType(Product.ProductType.PRODUCT)
                        .setService(Service.STOLYARKA)
                .setId(i++)
                .setName("Террасная доска Оптима 3000х142х27 мм лиственница")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/82270905.jpg")
                .setDescription("Террасная доска подходит для наружных работ. Доски используются для создания финишного покрытия поверхностей (монтируются на стену, пол и потолок) или в качестве самостоятельных конструктивных элементов.")
                .setProperties(Arrays.asList("Привлекательный вид — эффектное сочетание нескольких оттенков натурального дерева, красивая структура",
                        "Лиственница отличается высокой твердостью и прочностью, устойчивостью к атмосферным воздействиям",
                        "Доска не выделяет в воздух вредных веществ — изделию присвоен высокий класс безопасности в системе оценки летучих органических соединений (A)"))
                );

        products.put(i, new Product()
                        .setProductType(Product.ProductType.PRODUCT)
                        .setService(Service.STOLYARKA)
                .setId(i++)
                .setName("Фанера 10 мм ФК нешлифованная 1525x1525 мм сорт 4/4, 2.325 м2")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/12750175.jpg")
                .setDescription("Фанера ФК нешлифованная толщиной 10 мм применяется при ремонте и строительстве в качестве чернового отделочного материала. Используется для выравнивания поверхности, в виде подложки под мягкую кровлю или напольное покрытие. Выпускается в форме листа квадратной формы со стороной 152,5 см (площадь — 2,33 м^2^), удобного при транспортировке и монтаже. Производится в России.")
                .setProperties(Arrays.asList("Изготовлена из березового шпона 4-го сорта (самый недорогой сорт — поверхность может содержать сучки и дефекты кромок)",
                        "Проклеена безвредным карбамидным клеем, не выделяющим токсичных веществ",
                        "Имеет необработанную поверхность, совместимую со всеми видами чистовой отделки",
                        "Благодаря водоотталкивающей пропитке устойчива к воздействию влаги, может применяться для наружных работ",
                        "Не деформируется при сезонных колебаниях температуры и влажности",
                        "Выдерживает значительные механические нагрузки",
                        "Проста в обработке и монтаже"
                ))
                );


        //Слесарка
        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SLESARKA)
                .setId(i++)
                .setName("Лом-гвоздодер шестигранный Sparta 600 мм, сталь")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/12675291.jpg")
                .setDescription("Шестигранный гвоздодер предназначен для демонтажа деревянных и других конструкций, скрепленных посредством гвоздей. При помощи этого гвоздодера вы без труда удалите гвозди. Зубцы изделия закалены, что повышает его прочность. Гвоздодер покрыт антикоррозийным напылением, поэтому может работать и храниться в условиях повышенной влажности.")
                .setProperties(Arrays.asList("Надежность",
                        "Стойкость к ударным нагрузкам",
                        "Устойчивость к воздействию УФ-лучей"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SLESARKA)
                .setId(i++)
                .setName("Зубило по металлу 160х16 мм, оцинкованное")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/15495801.jpg")
                .setDescription("Швейцарское зубило по металлу является стержнем с рабочей частью, представленной резцом. Обратный конец оцинкованного зубила плоский, имеет в данном случае форму шестигранника. Во время работы с плоским зубилом Archimedes оно сперва устанавливается резцом на заготовку, после чего выполняются удары молотком или кувалдой. При этом должен быть хороший размах, который делается от плеча. Положение резца постоянно меняется зависимо от изначально задуманной цели.")
                .setProperties(Arrays.asList("Изготовлено из специальной стали, обладающей необходимыми характеристиками",
                        "Хорошо заточено, швейцарское качество",
                        "Цена, доступная любому потребителю"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SLESARKA)
                .setId(i++)
                .setName("Пила по металлу 300 мм")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/81983754.jpg")
                .setDescription("Пила по металлу комплектуется сменными полотнами длиной 300 мм. Ножовка способна резать металл, древесину толщиной до 50 мм, пластик, трубы из ПВХ. Используется при проведении слесарных, ремонтных или сантехнических работ. Производится в Китае")
                .setProperties(Arrays.asList("Полотно ножовки из углеродистой стали легко меняется благодаря подпружиненному креплению, снабженному гайкой-«барашком»",
                        "Комфортное пиление обеспечивает эргономичная пластиковая ручка",
                        "Вес инструмента составляет 270 г"
                ))
        );


        //Сварка
        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SVARKA)
                .setId(i++)
                .setName("Сварочный аппарат Arc-On 190 А")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/81928397.jpg")
                .setDescription("Сварочный аппарат Foxweld ARC-ON 190 — прибор для выполнения строительных задач: применяется для крепления металлических деталей и конструкций методом сварки. Работает на однофазном токе с напряжением 230 вольт. Предназначен для ручной дуговой сварки ММА.")
                .setProperties(Arrays.asList("Тип сварки — MMA",
                        "Повышенный уровень безопасности: прибор защищен от перегрева",
                        "Надежный металлический корпус вентилируется",
                        "Допустимый диаметр электрода — 4 мм",
                        "Функция \"антизалипания\" препятствует прокаливанию электрода: продлевается срок эксплуатации элемента",
                        "В комплекте: зажим на массу с кабелем, электрододержатель с кабелем",
                        "Мощность — 6600 Вт",
                        "Длина кабеля составляет 2 метра"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SVARKA)
                .setId(i++)
                .setName("Маска сварщика-хамелеон Сварис Оптима 3/11")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/14273376.jpg")
                .setDescription("«Сварис» — защитная маска сварщика, выполненная по принципу «хамелеон». Автоматическое затемнение светофильтра в самом начале сварки обеспечивает защиту глаз сварщика от светового излучения и в то же время позволяет ему контролировать зону сварки. В модели предусмотрена возможность регулировки чувствительности. Купив эту маску, можно устанавливать режим для условий конкретного типа сварки. Конструкция рассчитана на комбинированное питание: встроенный литиевый элемент и солнечные батареи.")
                .setProperties(Arrays.asList("Фвтоматическое усиление затемнения в начале сварки позволяет контролировать процесс без риска ослепления",
                        "Высокая степень защиты",
                        "Удобство использования"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SVARKA)
                .setId(i++)
                .setName("Электроды сталь МР-3С 3 мм 5 кг, цвет синий")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/18261559.jpg")
                .setDescription("Электроды предназначены для соединения низкоуглеродистых и рядовых сталей ручной дуговой сваркой постоянным или переменным током. Представляют собой металлические стержни (материал — сталь) длиной 350 мм и диаметром 3 мм. Рутиловое покрытие обеспечивает стабильное горение дуги независимо от типа тока. Цвет покрытия – синий. " +
                                "В упаковке 177 электродов общим весом 5 кг. Страна производства товара — Беларусь.")
                .setProperties(Arrays.asList("Могут использоваться для сварки влажного и плохо очищенного от ржавчины и прочих загрязнений металла",
                        "Позволяют делать швы, обладающие высокой ударной вязкостью",
                        "Не выделяют токсичных газов в процессе горения",
                        "Имеют низкий коэффициент разбрызгивания"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SVARKA)
                .setId(i++)
                .setName("Магнит для сварки под углом 90 градусов 34 кг")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/81928394.jpg")
                .setDescription("Магнит для сварки под углом 90 градусов — практичное приспособление, значительно упрощающее производственный процесс. При наличии такого инструмента сварщик может выполнять профессиональные операции, не привлекая подсобника. Магнитный угольник заменяет громоздкие струбцины и зажимы, обеспечивая исполнителю максимально комфортные условия для работы.")
                .setProperties(Arrays.asList("Вес — 1,035 кг",
                        "Рабочие углы — 45°, 90°, 135°",
                        "Удерживающая способность — до 34 кг"
                ))
        );

        products.put(i, new Product()
                .setProductType(Product.ProductType.PRODUCT)
                .setService(Service.SVARKA)
                .setId(i++)
                .setName("Сварочный аппарат инверторный Stanley Star 4000, 160 А, до 4 мм")
                .setImageSrc("https://res.cloudinary.com/lmru/image/upload/f_auto,q_auto,b_white,d_photoiscoming.png/LMCode/81929219.jpg")
                .setDescription("Сварочный аппарат инверторный Stanley Star 4000 — прибор для выполнения строительных задач: применяется для крепления металлических деталей и конструкций методом сварки. Работает на однофазном токе с напряжением 230 вольт. Предназначен для MMA — ручной дуговой сварки штучными электродами с покрытием.")
                .setProperties(Arrays.asList("Тип сварки — MMA",
                        "Повышенный уровень безопасности: прибор защищен от перегрева",
                        "Надежный металлический корпус вентилируется",
                        "Допустимый диаметр электрода — до 4 мм",
                        "Функция \"антизалипания\" препятствует прокаливанию электрода: продлевается срок эксплуатации элемента",
                        "В комплекте: зажим массы, держатель электрода, щетка-молоток, маска сварщика",
                        "Мощность — 5000 Вт",
                        "Длина кабеля составляет 2 метра",
                        "Сделано в Италии."
                ))
        );

        products.put(70, new Product()
                .setProductType(Product.ProductType.SERVICE)
                .setService(Service.SVARKA)
                .setId(70)
                .setName("Сварка")
                .setImageSrc("https://zdesinstrument.ru/wp-content/uploads/2017/11/svarka-26-1.jpg")
                .setDescription("Стоимость работы одного сварщика указана за рабочий день." +
                        "Специалисты имеют большой опыт в области проведения сварочных работ, используют только лучшие материалы и современное оборудование. Стоимость сварочных работ оговаривается индивидуально." +
                        "")
                .setProperties(Arrays.asList("На работу дается гарантия год",
                        "Работает специалист высшего разряда",
                        "Работа производится инструментами немецкого производства"
                ))
        );

    }

    private void mockPositions() {
        int i = 0;
        Position position;
        for (Organization org : organizations.values()) {
            for (Product product : products.values()) {
                if (org.getServices().contains(product.getService())) {
                    position = new Position()
                            .setId(i)
                            .setOrganization(org)
                            .setProduct(product)
                            .setPrice(new Random().nextInt(20 * (product.getId() + 4)) + 100 * product.getId());
                    positions.put(i++, position);
                    if (i % 3 == 0) {
                        users.get(MOCKED_USER_ID)
                                .getBookmarkedPositionsId()
                                .add(positions
                                        .get(i-1)
                                        .getId());
                    }
                }
            }
        }


        position = new Position()
                .setId(i)
                .setOrganization(organizations.get(4))
                .setProduct(products.get(70))
                .setPrice(6000);
        positions.put(i++, position);


        position = new Position()
                .setId(i)
                .setOrganization(organizations.get(5))
                .setProduct(products.get(70))
                .setPrice(2000);
        positions.put(i++, position);
    }

}
