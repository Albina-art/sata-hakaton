package ru.env.utils;

public enum PositionScoring {
    PRICE (0.5),
    ORGANIZATION_SCORE (0.5)
    ;

    PositionScoring(double weight) {
        this.weight = weight;
    }

    public final double weight;
}
