package ru.env.utils;

public class Location {

    public final double latitude;
    public final double longitude;
    public final String city;

    public Location(double latitude, double longitude, String city) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
    }
}
