package ru.env.utils;

public enum OrganizationScoring {
    DISTANCE (0.15),
    DUE_DILIGENCE_TRUST (0.34),
    REVIEWS_RATING (0.27),
    USER_LOYALTY (0.24)
    ;

    OrganizationScoring(double weight) {
        this.weight = weight;
    }

    public final double weight;
}
