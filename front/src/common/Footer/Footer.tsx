import React from 'react'

import './Footer.css'


export default function Footer() {
    return (
        <div className="footer">
            <div className="footer__wrapper">
                <div className="footer__text">
                    © 2007 — 2019 ПАО РОСБАНК, Генеральная лицензия №2272 от 28.01.15
                </div>
            </div>
        </div>
    )
}