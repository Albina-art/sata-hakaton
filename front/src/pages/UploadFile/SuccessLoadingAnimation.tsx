import React from 'react'

import { ReactComponent as AnimationSvg } from '../../images/animationSuccess.svg'


export default function SuccessLoadingAnimation() {
    return (
        <AnimationSvg />
    )
}