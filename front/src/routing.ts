/*eslint-disable */
export enum Routes {
    HOME = '/',
    PRODUCTS = '/products',
    SEARCH = '/search',
    PRODUCT = '/product/:positionId',
    NOT_FOUND = '/404',
}